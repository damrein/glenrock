# README #

This project was for a made up golf course named Duncan Amrein Golf Course
I took the 4 best elements for a golf course site and displayed them in seperate tabs, including:

    Home Page: Generic course picture, golf rates, and score card
    About Page: Clubhouse picture and brief desciption
    Course Photos: Pictures of different holes on course w description
    Contact: How we can be reached through phone and a clickable email link

Included clickable images to twitter and facebook on every footer page, along with copyright

Included media and viewport to my contact page to display knowledge of requirement

Used bootstrap for dynamic page sizing with mobile first approach

Linked each html file to its own css file

For my header I used the navbar element to navigate through the site

Fixed closing tags that were incorrect on each page

Added more comments throught code to explain actions

Even though project is relatively plain, the project is about making the website
as simple as possible without clutter to make nagivating as easy and straight forward
as possible.
